//1. С помощью javascript найти в таблице ячейки, в которых написаны числа 2, 4 и 6. 
//Найденным ячейкам установить фоновый цвет: #333333, а цвет текста: #FFFFFF; 
 
var table = document.getElementsByTagName('td');
  for(var i=0; i < table.length; i++){
    if(i % 2 != 0){
      table[i].style.backgroundColor = '#333333';
      table[i].style.color = '#ffffff';
    }
  }

//2. С помощью javascript найти и поменять местами текстовое содержимое
// 1 и 3 блоков: "1. Текст блока №1" и "3. Текст блока №3".
var firstBar = document.querySelectorAll('.first')[0]; 
var lastBar = document.querySelectorAll('.last')[0]; 
var firstText = firstBar.firstChild;
var lastText = lastBar.firstChild;
firstBar.replaceChild(lastText, firstText);
lastBar.prepend(firstText);

//3.С помощью javascript найти и удалить блок с текстом "2. Текст блока №2".

var totalBlock = document.querySelectorAll('.removeSecondBlockHere')[0];
var firstBlock = totalBlock.firstChild.nextSibling;
var delBlock = firstBlock.nextSibling.nextSibling;
totalBlock.removeChild(delBlock);


