var form = document.querySelector('#form');
var btnEnter = document.querySelector('.btn-enter');
var btnRegistration = document.querySelector('.btn-registration');
var btnLogout = document.querySelector('.btn-logout');
 
 
form.onsubmit = function(event){
	event.preventDefault();
} 	
	
btnEnter.addEventListener('click', function(){
		form.classList.add('hide');
		
		var email = document.querySelector('#email').value;
	   var pass = document.querySelector("#pass").value;
		var body = 'email=' + encodeURIComponent(email) + 'password=' + encodeURIComponent(pass);
	 	
		var url = 'http://netology-hj-ajax.herokuapp.com/homework/login_json';	 
		var xhr = new XMLHttpRequest();
		xhr.open('POST', url, true);
//		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');	 
		xhr.addEventListener('readystatechange', function () {
	       if (xhr.status >= 200 && xhr.status <= 299) {
				//	console.log('point1');
		 			var profile = document.querySelector('.profile');
					form.classList.add('hide');
					profile.classList.add('show');

		         var user = JSON.parse(xhr.responseText);

					var avatar = document.querySelector('img');
					avatar.src = user.userpic;
					profile.innerHTML = avatar;

					var name = document.querySelector('.name-profile');
					name.innerHTML = user.name + ', ' + user.lastname;
					
					var country = document.querySelector('.country-profile');
					country.innerHTML = user.country;
					
					var hobbies = document.querySelector('.hobbies-profile');
					hobbies.innerHTML = user.hobbies;
					
					var logout = document.querySelector('.btn-logout');
					var preloader = document.querySelector('.preloader');
					var form = document.querySelector('#form');
					preloader.classList.add('hide');
					form.classList.add('hide');
					logout.classList.add('show-strong');
			}
			else{
			//	console.log('point2');
				var form = document.querySelector('#form');
				var message = document.querySelector('.warning-txt');
				message.classList.add('show');
	         var warning = JSON.parse(xhr.responseText);
				message.innerHTML = warning.error.message;
				form.classList.add('show');
			}
		});
	 
		var preloader = document.querySelector('.preloader');
		xhr.addEventListener('loadstart', function(){
			preloader.classList.add('show');
		}); 
		xhr.addEventListener('loadend', function(){
			preloader.classList.add('hide-strong');
		}); 
		xhr.send(body);
});
		

var form = document.querySelector('#form');
var logout = document.querySelector('.btn-logout');
logout.addEventListener('click', function(){
	form.classList.add('show');
	logout.classList.add('hide-strong');
});	